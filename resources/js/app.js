import React, { Component } from 'react';
import axios from 'axios';
import Profile from './components/Profile';
import Album from './components/Album';
import './App.scss';

class App extends Component {
    constructor() {
        super();

        this.state = {
            photographer: null
        };
    }

    componentDidMount() {
        axios.get('/landscapes.json')
            .then(response => {
                this.setState({
                    photographer: response.data
                });
            })
            .catch(error => {
                // Show error to user
            });
    }

    render() {
        const photographer = this.state.photographer;

        return (
            <div className="app" role="main">
                <div className="container">
                    <Profile photographer={photographer} />
                    {photographer &&
                        <Album album={photographer.album} />
                    }
                </div>
            </div>
        );
    }
}

export default App;
