import React from 'react';
import Photo from './Photo';

const Album = props => {
    const photos = props.album.map(photo => <Photo key={photo.id} photo={photo} />);

    return (
        <div className="album">
            <div className="row">{photos}</div>
        </div>
    );
};

export default Album;
