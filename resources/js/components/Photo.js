import React from 'react';
import './Photo.scss';

const Photo = props => {
    const photoDetails = props.photo;
    const photo = require('../' + photoDetails.img);

    return (
        <div className="col-12 col-md-6 col-lg-4 photo-col">
            <div className="card">
                <div className="card-img-container">
                    <img className="card-img-top" src={photo} alt={photoDetails.title} />
                    <h3 className="h5">{photoDetails.title}</h3>
                </div>
                <div className="card-body">
                    <p className="card-text">{photoDetails.description}</p>
                    <div className="photo-date-container">
                        {photoDetails.featured &&
                            <span className="photo-featured">
                                <i className="fas fa-heart" title="Featured" aria-hidden="true"></i>
                                <span className="sr-only">Featured</span>
                            </span>
                        }
                        <span className="photo-date">{photoDetails.date}</span>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Photo;
