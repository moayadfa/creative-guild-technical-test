import React from 'react';
import './Profile.scss';

const Profile = (props) => {
    const photographer = props.photographer;
    const photo = photographer ? require('../' + photographer.profile_picture) : null;

    return (
        <div className="profile">
            <div className="row profile-row">
                <div className="col-12 col-lg-auto profile-photo-col">
                    <div className="photo-container">
                        {photographer &&
                            <img src={photo} alt="Nick Reynolds" />
                        }
                    </div>
                </div>
                {photographer &&
                    <div className="col-12 col-lg-auto profile-details-col">
                        <h1>{photographer.name}</h1>
                        <div className="row profile-details-row">
                            <div className="col-auto profile-bio-col">
                                <div className="bio-container">
                                    <h2 className="h6">Bio</h2>
                                    <p>{photographer.bio}</p>
                                </div>
                            </div>
                            <div className="col-auto profile-contact-col">
                                <div className="phone-number-container">
                                    <h2 className="h6">Phone</h2>
                                    <a href={'tel:' + photographer.phone}>{photographer.phone}</a>
                                </div>
                                <div className="email-container">
                                    <h2 className="h6">Email</h2>
                                    <a href={'mailto:' + photographer.email}>{photographer.email}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </div>
        </div>
    );
};

export default Profile;
