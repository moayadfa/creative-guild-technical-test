<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photographer extends Model
{
    protected $with = ['album'];
    protected $hidden = ['created_at', 'updated_at'];

    public function album()
    {
        return $this->hasMany('App\Photo');
    }
}
