<?php

namespace App\Http\Controllers;

use App\Photographer;
use Illuminate\Http\Request;

class PhotographerController extends Controller
{
    /**
     * Returns the specified photographer.
     *
     * @param  \App\Photographer  $photographer
     * @return \Illuminate\Http\Response
     */
    public function byId(Photographer $photographer)
    {
        return $photographer;
    }
}
